window.addEventListener("DOMContentLoaded", () => {
    const accordions = document.querySelectorAll(".accordion-item");

    accordions.forEach((elem) => {
        const button = elem.querySelector(".accordion-button");
        const content = elem.querySelector(".accordion-content-container");

        button.addEventListener("click", () => {
            const isActive = elem.classList.contains("active");
            // elem.parentNode.querySelectorAll(".accordion-item").forEach((a) => {
            //     a.querySelector(".accordion-content").style.maxHeight = "0px";
            //     a.classList.remove("active");
            // });
            if(isActive) {
                elem.classList.remove("active");
                content.style.maxHeight = "0px";
            } else {
                elem.classList.add("active");
                content.style.maxHeight = `${content.scrollHeight}px`;
            }
            
        });
    });
});
