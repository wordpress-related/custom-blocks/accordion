import { registerBlockType } from "@wordpress/blocks";
import { useBlockProps, InnerBlocks, RichText } from "@wordpress/block-editor";

const blockStyle = {
    border: "1px solid #999",
    borderRadius: 5,
    padding: "20px",
    paddingTop: "60px",
    maxWidth: "1020px",
    margin: "12px auto",
    position: "relative"
};

const innerBlockStyle = {
    boxShadow: "0px 0px 20px -11px #888",
    borderRadius: 4,
    padding: "20px",
    marginBottom: "10px",
};

const blockTitleStyle = {
    position: "absolute",
    width: "100%",
    padding: "8px",
    background: "#777",
    color: "#fff",
    top: 0,
    left: 0,
    fontSize: "14px"
}

registerBlockType('custom-blocks/accordion-block', {
    apiVersion: 2,
    title: 'Accordion',
    icon: 'list-view',
    category: 'design',
    edit: (props) => {
        const blockProps = useBlockProps();
        return (
            <div {...blockProps} style={blockStyle}>
                <div style={blockTitleStyle}>Accordion Block</div>
                <InnerBlocks
                    allowedBlocks={["custom-blocks/accordion-block-child"]}
                    template={[["custom-blocks/accordion-block-child"]]}
                />
            </div>
        );
    },
    save: () => {
        return <InnerBlocks.Content />;
    },
});

registerBlockType("custom-blocks/accordion-block-child", {
    apiVersion: 2,
    title: "Accordion Item",
    icon: "list-view",
    category: "design",
    attributes: {
        title: {
            type: "string",
        },
    },
    parent: ["custom-blocks/accordion-block"],
    edit: (props) => {
        const { setAttributes } = props;
        const blockProps = useBlockProps();
        const onChangeTitle = (newTitle) => {
            setAttributes({ title: newTitle });
        };
        return (
            <div {...blockProps} style={innerBlockStyle}>
                <div>
                    <RichText
                        tagName="h3"
                        className="custom-block-accordion-item-title"
                        value={props.attributes.title}
                        onChange={onChangeTitle}
                        style={{
                            borderBottom: "1px solid",
                        }}
                        placeholder="Accordion title"
                    />
                </div>
                <InnerBlocks />
            </div>
        );
    },
    save: () => {
        return <InnerBlocks.Content />;
    },
});
