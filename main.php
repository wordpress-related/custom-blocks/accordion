<?php

class AccordionBlock
{

    private $items = [];

    public static function init()
    {
        $class = __CLASS__;
        new $class;
    }

    function __construct()
    {

        // automatically load dependencies and version
        $asset_file = include(plugin_dir_path(__FILE__) . 'build/index.asset.php');

        wp_register_script(
            'custom-blocks-accordion-block-script',
            path_join(get_template_directory_uri(), 'inc/accordion/build/index.js'),
            $asset_file['dependencies'],
            $asset_file['version']
        );

        wp_register_script(
            'custom-blocks-accordion-block-frontend-script',
            path_join(get_template_directory_uri(), 'inc/accordion/dist/frontend.js'),
            [],
            filemtime(path_join(get_template_directory(), 'inc/accordion/dist/frontend.js'))
        );


        wp_register_style(
            'custom-blocks-accordion-block-frontend-style',
            path_join(get_template_directory_uri(), 'inc/accordion/dist/frontend.css'),
            [],
            filemtime(path_join(get_template_directory(), 'inc/accordion/dist/frontend.css'))
        );


        register_block_type('custom-blocks/accordion-block', array(
            'apiVersion' => 2,
            'editor_script' => 'custom-blocks-accordion-block-script',
            'render_callback' => function ($block_attributes, $content) {
                return $this->block_render_callback($block_attributes, $content);
            },
            'script' => 'custom-blocks-accordion-block-frontend-script',
            'style' => 'custom-blocks-accordion-block-frontend-style'
        ));

        register_block_type('custom-blocks/accordion-block-child', array(
            'apiVersion' => 2,
            'editor_script' => 'custom-blocks-accordion-block-script',
            'render_callback' => function ($block_attributes, $content) {
                return $this->block_child_render_callback($block_attributes, $content);
            }
        ));
    }

    private function block_child_render_callback($block_attributes, $content)
    {
        $this->items[] = [
            "attributes" => $block_attributes,
            "content" => $content
        ];
        return $content;
    }

    private function block_render_callback($block_attributes, $content)
    {
        $accordion_icons = <<<HTML
        <div class="accordion-icons">
            <div class="icon arrow-down">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><polyline points="6 9 12 15 18 9"/></svg>
            </div>
            <div class="icon arrow-up">
                <svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" ><polyline points="18 15 12 9 6 15"></polyline></svg>
            </div>
        </div>
        HTML;

        $data = [];
        foreach ($this->items as $index => $block_item) {
            $data[] = "<div class='accordion-item" . ($index === 0 ? " active" : "") . "'><button class='accordion-button'>" . $block_item["attributes"]["title"] . $accordion_icons . "</button><div class='accordion-content-container'><div class='accordion-content'>" . $block_item["content"] . "</div></div></div>";
        }
        $this->items = [];

        return "<div class='accordion-block-container'>" . join("", $data) . "</div>";
    }
}
